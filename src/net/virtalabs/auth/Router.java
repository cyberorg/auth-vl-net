package net.virtalabs.auth;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.mod.auth.Auth;
import net.virtalabs.auth.mod.checkToken.CheckTokenStruct;
import net.virtalabs.auth.mod.register.RegisterStruct;
import net.virtalabs.auth.struct.StdRxStruct;
import net.virtalabs.auth.utils.C;


public class Router {

	public static String route(String json) throws AppException {
			//test if json if not empty
			if(json.length()==0){
				throw new AppException("Provided JSON is empty",C.EMPTY_JSON);
			}
			//Rx
			String action = Router.getAction(json);

			if(action.equals("auth")){
				//auth
				return Auth.run(json);
			} else if(action.equals("register")){
				//register
				RegisterStruct modData = (RegisterStruct) StdRx.read(json,RegisterStruct.class);
				//TODO data checker
				return net.virtalabs.auth.mod.register.Register.run(modData);
			} else if(action.equals("checkToken")) {
				//checkToken
				CheckTokenStruct modData = (CheckTokenStruct) StdRx.read(json, CheckTokenStruct.class);
				return net.virtalabs.auth.mod.checkToken.CheckToken.run(modData);
			} else {
				throw new AppException("No such action here",C.NO_SUCH_ACTION);
			}
	}
	 private static String getAction(String json) throws AppException{	
		try{
			StdRxStruct parsedJson = StdRx.read(json, StdRxStruct.class);
			String action = parsedJson.getAction();
			if(action == null){
				throw new NullPointerException();
			}
			return action;
		}catch (NullPointerException npe) {
			throw new AppException("There is no action key in JSON. See API",C.WRONG_JSON);
		}
	 }

}
