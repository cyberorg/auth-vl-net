package net.virtalabs.auth.mod.checkToken;

import com.google.gson.Gson;

public class Tx {
	private static final CheckTokenTxStruct sTx = new CheckTokenTxStruct();
	private static final Gson gson = new Gson();
	
	static String talk(String result){
		//TODO replace stub with real data from result 
		int uid = 11;
		int expiry=  102359350;
		
		sTx.setCode(0);
		sTx.setMessage("Sending data");
		
		CheckTokenTxStruct.Data data = new CheckTokenTxStruct.Data();
		data.setUid(uid);
		data.setExpiry(expiry);
		sTx.setData(data);
		//and return
		return gson.toJson(sTx);
	}
}
