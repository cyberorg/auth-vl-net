package net.virtalabs.auth.mod.checkToken;

import net.virtalabs.auth.struct.StdTxStruct;

public class CheckTokenTxStruct extends StdTxStruct {
	private Data data;
	
	static class Data{
		private int uid;
		private int expiry;
		public int getUid() {
			return uid;
		}
		public int getExpiry() {
			return expiry;
		}
		public void setUid(int uid) {
			this.uid = uid;
		}
		public void setExpiry(int expiry) {
			this.expiry = expiry;
		}
		
		
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
}
