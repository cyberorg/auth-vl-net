package net.virtalabs.auth.mod.auth;

import net.virtalabs.auth.StdRx;
import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.mod.auth.google.GoogleAuthStruct;
import net.virtalabs.auth.mod.auth.self.SelfAuthStruct;
import net.virtalabs.auth.utils.C;

public class Auth {
	public static String run(String rawData) throws AppException{
		//internal routing
		AuthStruct appData = (AuthStruct) StdRx.read(rawData, AuthStruct.class);
		String provider = appData.getProvider();
		if(provider.equals("self")){
			SelfAuthStruct modData = (SelfAuthStruct) StdRx.read(rawData, SelfAuthStruct.class);
			return net.virtalabs.auth.mod.auth.self.SelfAuth.run(modData);
		} else if(provider.equals("google")){
			GoogleAuthStruct modData = (GoogleAuthStruct) StdRx.read(rawData, GoogleAuthStruct.class);
			return net.virtalabs.auth.mod.auth.google.GoogleAuth.run(modData);
		} else {
			throw new AppException("Provider unsupported yet",C.NO_SUCH_PROVIDER);
		}
	}
}
