package net.virtalabs.auth.mod.auth;

import net.virtalabs.auth.struct.StdRxStruct;

public class AuthStruct extends StdRxStruct {
	protected String provider;

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	
}
