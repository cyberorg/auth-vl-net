package net.virtalabs.auth.mod.auth.google;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.utils.C;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

public class Rx {
	private static final Gson gson = new Gson();
	
	static GoogleAuthStruct read(String input) throws AppException{
		try{
			GoogleAuthStruct data = gson.fromJson(input, GoogleAuthStruct.class);
			return data;
		}catch (JsonParseException jpe){
			throw new AppException("Failed to parse json",C.WRONG_JSON);
		}
	}
}
