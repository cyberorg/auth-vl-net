package net.virtalabs.auth.mod.auth.google;

import net.virtalabs.auth.mod.auth.AuthStruct;

public class GoogleAuthStruct extends AuthStruct {
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
