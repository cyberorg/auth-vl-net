package net.virtalabs.auth.mod.auth.google;

import net.virtalabs.auth.struct.StdTxStruct;

public class GoogleAuthTxStruct extends StdTxStruct {
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
