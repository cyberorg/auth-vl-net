package net.virtalabs.auth.mod.auth.google;

import net.virtalabs.auth.exceptions.AppException;

public class Pre {
	static GoogleAuthStruct prepareData(String input) throws AppException{
		//Rx
		GoogleAuthStruct data = Rx.read(input);
		// no checker needed
		return data;
	}
}
