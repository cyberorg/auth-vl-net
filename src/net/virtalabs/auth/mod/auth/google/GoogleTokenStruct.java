package net.virtalabs.auth.mod.auth.google;

public class GoogleTokenStruct {
	private String access_token;
	private String token_type;
	private int expires_in;
	private String id_token;

	public String getAccessToken() {
		return access_token;
	}

	public String getTokenType() {
		return token_type;
	}

	public int getExpiresIn() {
		return expires_in;
	}

	public String getIdToken() {
		return id_token;
	}

}
