package net.virtalabs.auth.mod.auth.google;

public class GoogleUserInfoStruct {
	private String id;
	private String email;
	private boolean verified_email;

	private String name;
	private String given_name;
	private String family_name;
	// Google+
	private String link;
	private String gender;
	private String locale;
	private String hd;

	public String getGivenName() {
		return given_name;
	}

	public String getFamilyName() {
		return family_name;
	}

	public String getEmail() {
		return email;
	}

}
