package net.virtalabs.auth.mod.auth.google;

import net.virtalabs.auth.StdTx;
import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.utils.C;

import com.google.gson.Gson;

public class GoogleAuth {
	private static Gson gson = new Gson();

	public static String run(GoogleAuthStruct appData) throws AppException {
		// 0)get code
		String code = appData.getCode();
		// 1)make request to Google Server (POST)
		String postBody = NetWork.makeLink(code);
		String tokenJson = NetWork.getAccessToken(postBody);
		if (tokenJson == null) {
			throw new AppException("Google is silent", C.GENERAL_ERROR);
		}
		// 2)handle Reply (access_token, expires_in)

		GoogleTokenStruct tokenStruct = gson.fromJson(tokenJson,
				GoogleTokenStruct.class);

		// 3) request to Google API and get User Info (email, name, surname)
		String authInfo = NetWork.getUserInfo(tokenStruct.getAccessToken(),
				tokenStruct.getTokenType());

		GoogleUserInfoStruct userInfoStruct = gson.fromJson(authInfo,
				GoogleUserInfoStruct.class);
		int expiryTS = (int) (System.currentTimeMillis() / 1000L)
				+ tokenStruct.getExpiresIn();

		String message = "E-mail: " + userInfoStruct.getEmail()
				+ " First Name: " + userInfoStruct.getGivenName()
				+ " Last Name: " + userInfoStruct.getFamilyName() + " Token: "
				+ tokenStruct.getAccessToken() + " Expiry " + expiryTS;

		return StdTx.reply(0, message);
		// 5)find out that user exists ? goto 7 : goto 6

		// 6)create new record if not (store current user info, token, expiry to
		// DB)
		// 7)update token and expiry
		// 8)return token

		// return stub
		// return "";

	}
}
