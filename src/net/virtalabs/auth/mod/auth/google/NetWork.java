package net.virtalabs.auth.mod.auth.google;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.utils.C;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

public class NetWork {
	static String makeLink(String code) throws AppException {
		// config
		String config = "cnf/googleApp.json";
		// keys
		String ClientId = "client_id";
		String ClientSecret = "client_secret";
		String RedirectUri = "redirect_uri";
		String GrantType = "grant_type";
		String codeKey = "code";
		try {
			FileReader reader = new FileReader(config);
			// parse it to HashMap<String,String>
			Gson gson = new Gson();
			Type type = new TypeToken<HashMap<String, String>>() {
			}.getType();
			HashMap<String, String> cnf = gson.fromJson(reader, type);
			// create response String
			return codeKey + "=" + code + "&" + ClientId + "="
					+ cnf.get(ClientId) + "&" + ClientSecret + "="
					+ cnf.get(ClientSecret) + "&" + RedirectUri + "="
					+ cnf.get(RedirectUri) + "&" + GrantType + "="
					+ cnf.get(GrantType);

		} catch (FileNotFoundException fileEx) {
			throw new AppException(
					"Cannot find config file. Did you copy it to server?",
					C.CONFIG_ERROR);
		} catch (JsonParseException jpe) {
			throw new AppException("Cannot parse Config. Check it manually",
					C.CONFIG_ERROR);
		}
	}

	static String getAccessToken(String payload) throws AppException {
		String googleURL = "https://accounts.google.com/o/oauth2/token";
		BufferedReader reader = null;
		OutputStreamWriter writer = null;
		HttpURLConnection conn = null;
		try {
			URL url = new URL(googleURL);
			conn = (HttpURLConnection) url.openConnection();

			conn.setDoOutput(true);
			conn.setRequestMethod("POST");

			writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(payload);
			writer.flush();
			String buffer = null;
			String result = "";
			reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			while ((buffer = reader.readLine()) != null) {
				result = result + buffer;
				// System.out.println(line);
			}
			writer.close();
			reader.close();
			return result;
		} catch (MalformedURLException e) {
			throw new AppException("Got Network-related error: wrong URL",
					C.NET_ERROR);
		} catch (IOException e) {
			StringBuffer errBuffer = new StringBuffer();
			try {
				System.err.println("Code: " + conn.getResponseCode());
				InputStream is = conn.getErrorStream();
				byte[] data = new byte[is.available()];
				is.read(data);
				String body = new String(data);
				System.err.println("Google said: " + body);
				// close flows
				if (is != null) {
					is.close();
				}
				if (writer != null) {
					writer.close();
				}
				if (reader != null) {
					reader.close();
				}
				// make meanful reply in AppException
				Gson gson = new Gson();
				GoogleErrStruct jsonWithError = gson.fromJson(reader,
						GoogleErrStruct.class);
				errBuffer.append(jsonWithError.getError());
				System.err.println("Request was: " + payload);
			} catch (IOException e1) {
				throw new AppException(
						"Got Network-related error: Net I/O while reading response",
						C.NET_ERROR);
			} catch (NullPointerException npe) {
				throw new AppException(
						"Got Network-related error: Net I/O while reading response aka NPE",
						C.NET_ERROR);
			}
			throw new AppException("Got " + errBuffer.toString()
					+ " error from google", C.NET_ERROR);
		}

	}

	static String getUserInfo(String token, String auth) throws AppException {
		String googleURL = "https://www.googleapis.com/oauth2/v2/userinfo";
		// making payload, which acts as header here
		// String payload = auth + " " + token;
		String payload = "OAuth " + token;
		BufferedReader reader = null;
		// OutputStreamWriter writer = null;
		HttpURLConnection conn = null;
		try {
			URL url = new URL(googleURL);
			conn = (HttpURLConnection) url.openConnection();

			conn.setDoOutput(true);
			conn.setRequestMethod("GET");

			// sending Header
			conn.setRequestProperty("Authorization", payload);
			String buffer = null;
			String result = "";
			reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			while ((buffer = reader.readLine()) != null) {
				result = result + buffer;
			}
			reader.close();
			return result;
		} catch (MalformedURLException e) {
			throw new AppException("Got Network-related error: wrong URL",
					C.NET_ERROR);
		} catch (IOException e) {
			StringBuffer errBuffer = new StringBuffer();
			try {
				System.err.println("Code: " + conn.getResponseCode());
				InputStream is = conn.getErrorStream();
				byte[] data = new byte[is.available()];
				is.read(data);
				String body = new String(data);
				System.err.println("Google said: " + body);
				// close flows
				if (is != null) {
					is.close();
				}
				if (reader != null) {
					reader.close();
				}
				// make meanful reply in AppException
				/*
				 * Gson gson = new Gson(); GoogleErrStruct jsonWithError =
				 * gson.fromJson(reader, GoogleErrStruct.class);
				 * errBuffer.append(jsonWithError.getError());
				 */
				System.err.println("Header sent was: " + payload);
			} catch (IOException e1) {
				throw new AppException(
						"Got Network-related error: Net I/O while reading response",
						C.NET_ERROR);
			} catch (NullPointerException npe) {
				throw new AppException(
						"Got Network-related error: Net I/O while reading response aka NPE",
						C.NET_ERROR);
			}
			throw new AppException("Got " + errBuffer.toString()
					+ " error from google", C.NET_ERROR);
		}

		// return "";
	}
}
