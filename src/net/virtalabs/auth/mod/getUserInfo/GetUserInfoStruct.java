package net.virtalabs.auth.mod.getUserInfo;

import net.virtalabs.auth.struct.StdRxStruct;

public class GetUserInfoStruct extends StdRxStruct {
	private Data data;
	
	static class Data{
		private int uid;
		private String token;
		public int getUid() {
			return uid;
		}
		public String getToken() {
			return token;
		}

	}

	public Data getData() {
		return data;
	}
}
