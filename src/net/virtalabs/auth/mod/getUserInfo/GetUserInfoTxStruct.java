package net.virtalabs.auth.mod.getUserInfo;

import net.virtalabs.auth.struct.StdTxStruct;

@SuppressWarnings("unused")
public class GetUserInfoTxStruct extends StdTxStruct {
	private Data data;
	static class Data{
		
		private String name;
		private String surname;
		public void setName(String name) {
			this.name = name;
		}
		public void setSurname(String surname) {
			this.surname = surname;
		}
	}
	public void setData(Data data) {
		this.data = data;
	}
}
