package net.virtalabs.auth.mod.getUserInfo;

import com.google.gson.Gson;

public class Tx {
	private static final GetUserInfoTxStruct sTx = new GetUserInfoTxStruct();
	private static final Gson gson = new Gson();
	
	static String talk(String result){
		//TODO replace stub with real data from result 
		String name = "a";
		String surname = "b";
		
		sTx.setCode(0);
		sTx.setMessage("Sending data");
		
		GetUserInfoTxStruct.Data data = new GetUserInfoTxStruct.Data();
		data.setName(name);
		data.setSurname(surname);
		sTx.setData(data);
		//and return
		return gson.toJson(sTx);
	}
}
