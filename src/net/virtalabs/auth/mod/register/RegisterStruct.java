package net.virtalabs.auth.mod.register;

import net.virtalabs.auth.struct.StdRxStruct;

public class RegisterStruct extends StdRxStruct {
	private Data data;
	static class Data{
		private String name;
		private String pass;
		public String getName() {
			return name;
		}
		public String getPass() {
			return pass;
		}
	}
	public Data getData() {
		return data;
	}
}
