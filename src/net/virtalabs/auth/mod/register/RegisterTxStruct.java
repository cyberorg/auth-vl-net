package net.virtalabs.auth.mod.register;

import net.virtalabs.auth.struct.StdTxStruct;

@SuppressWarnings("unused")
public class RegisterTxStruct extends StdTxStruct {
	private Data data;
	static class Data{
		private String token;

		public void setToken(String token) {
			this.token = token;
		}
	}
	public void setData(Data data) {
		this.data = data;
	}
}
