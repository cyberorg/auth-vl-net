package net.virtalabs.auth;

import java.lang.reflect.Type;

import net.virtalabs.auth.exceptions.AppException;
import net.virtalabs.auth.struct.StdRxStruct;
import net.virtalabs.auth.utils.C;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

public class StdRx {
	private static final Gson gson = new Gson();
	
	 public static StdRxStruct read(String json,Type type) throws AppException{
		 try{
		 return gson.fromJson(json,type);
		 }catch(JsonParseException jpe){
			 throw new AppException("Param must be valid JSON. See API",C.WRONG_JSON);
		 }
	 }
}
