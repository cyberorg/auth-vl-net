package net.virtalabs.auth.exceptions;

import net.virtalabs.auth.StdTx;


public class AppException extends Exception {

	private static final long serialVersionUID = 1999769197223372522L;
	private int code;
	private String message;
	
	public AppException(String message,int code){
		this.code = code;
		this.message = message;
	}
	
	public String handle(){
		return StdTx.reply(this.code,this.message);
	}
}
