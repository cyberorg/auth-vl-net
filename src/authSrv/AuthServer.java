package authSrv;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.IOException;
 
import net.virtalabs.auth.App;

import org.eclipse.jetty.server.Server;

import org.eclipse.jetty.servlet.*;

import authSrv.ServerConf;


public class AuthServer {
	public static void main(String[] args) throws Exception
    {
		int port = 0;
		try{
		port = ServerConf.getPort();
		}catch(Exception e){
			System.err.println("Cannot start server. Reason: Cannot read port from config. Make sure you have server config json in cnf folder. ");
			e.printStackTrace(System.err);
			System.exit(1);
		}
        Server server = new Server(port);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
 
        context.addServlet(new ServletHolder(new AuthServlet()),"/*");
        server.start();
        server.join();
    }
  static public class AuthServlet extends HttpServlet
{

	private static final long serialVersionUID = 1L;
    public AuthServlet(){}
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {	
    	//in
    	try{
    	String data = request.getParameter("data");
    	String token = request.getParameter("access_token");
    	//app
    	StringBuffer rslt = new StringBuffer();
    	
    	if(data != null){
    		if(!data.equals("")){
    			//app 
        		rslt.append(App.run(data));   
        		AuthServlet.reply(response, rslt);
    		} else {
    			//400
    			AuthServlet.reply(response,HttpServletResponse.SC_BAD_REQUEST);
    		}
    		
    	} else if(token != null){
    		if(!token.equals("")){
    			AuthServlet.reply(response, token);
    		} else {
    			//400
    			AuthServlet.reply(response,HttpServletResponse.SC_BAD_REQUEST);
    		}
    	} else {
    		//Nothing above recieved
    		System.err.println(request.getQueryString()); //just for debug
    		AuthServlet.reply(response,HttpServletResponse.SC_BAD_REQUEST);
    	}

    	}catch(NullPointerException e){
    		AuthServlet.reply(response,HttpServletResponse.SC_BAD_REQUEST);
    	}catch (Exception e) {
    		 e.printStackTrace();
    		 AuthServlet.reply(response, HttpServletResponse.SC_SERVICE_UNAVAILABLE);
		}
    }
    private static void reply(HttpServletResponse response, StringBuffer rslt) throws IOException{
    	String result = rslt.toString();
    	AuthServlet.reply(response, result);
    }
    private static void reply(HttpServletResponse response, String result) throws IOException{
    	response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println(result);
    }
    private static void reply(HttpServletResponse response, int status) throws IOException{
        response.setContentType("text/html");
	    response.setStatus(status);
	    response.getWriter().println();
    }
}

} 
