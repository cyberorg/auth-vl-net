package authSrv;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.MalformedJsonException;

public class ServerConf {
	
	private static ServerConf self = null;
	
	private Gson gson = new Gson();
	private Type type = new TypeToken<HashMap<String,String>>(){}.getType();
	
	private HashMap<String,String>cnf = null;
	//Config
	private String srvCnf = "cnf/Server.json";
	//Keys
	private String PORT = "port";
	
	private ServerConf() throws FileNotFoundException{
			FileReader reader = new FileReader(srvCnf);
			this.cnf=this.gson.fromJson(reader,this.type);
		
	}
	
	
	public static int getPort() throws FileNotFoundException, MalformedJsonException,NullPointerException{
		//init object
		if(self==null){
			self = new ServerConf();
		}
		return self.readPort();
	}
	private int readPort() throws MalformedJsonException,NullPointerException{
		if(this.cnf==null){
			throw new NullPointerException("No cnf. ");
		}
		String portStr = this.cnf.get(PORT);
		if(portStr==null){
			throw new MalformedJsonException("");
		}
		return Integer.parseInt(portStr);
	}
}
